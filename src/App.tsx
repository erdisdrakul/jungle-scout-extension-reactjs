import React from 'react';
import './App.css';
import SearchComponent from './components/SearchComponent/SearchComponent';

const App: React.FC = () => {
    return (
        <SearchComponent />
    );
}

export default App;
