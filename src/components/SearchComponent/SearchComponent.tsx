import * as React from 'react';
import './SearchComponent.scss';

import $ from 'jquery';
import { } from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import { SearchProducts } from './../../services/SearchProductsService';
import ProductModel from '../../models/ProductModel';

export interface IProps { }

interface IState {
    query: string;
    currentPage: number;
    isLoading: boolean;
    products: ProductModel[];
    averageDailySales: number;
    averageSalesRank: number;
    averagePrice: number;
    averageReviews: number;
}

export default class SearchComponent extends React.Component<IProps, IState> {
    private readonly currencyFormat = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumIntegerDigits: 1, maximumFractionDigits: 2 });

    constructor(props: IProps) {
        super(props);

        this.state = {
            query: '',
            currentPage: 0,
            isLoading: false,
            products: [],
            averageDailySales: 0,
            averageSalesRank: 0,
            averagePrice: 0,
            averageReviews: 0
        }

        this.generateDialogHtml = this.generateDialogHtml.bind(this);
        this.onSearchEnter = this.onSearchEnter.bind(this);
        this.onHandleChange = this.onHandleChange.bind(this);
        this.onNextPageClick = this.onNextPageClick.bind(this);
        this.onClearSearchClick = this.onClearSearchClick.bind(this);
    }

    componentDidUpdate() {
        ($('tbody td button') as any).popover({
            trigger: 'focus'
        });
    }

    onSearchEnter(e: any) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            e.preventDefault();
            this.setState({
                isLoading: true,
                currentPage: 0
            }, () => this.searchProducts());
        }
    }

    onHandleChange(e: any) {
        this.setState({ query: e.target.value });
    }

    onNextPageClick() {
        this.setState({
            isLoading: true,
            currentPage: this.state.currentPage + 1
        }, () => this.searchProducts());
    }

    onClearSearchClick() {
        this.setState({
            query: '',
            currentPage: 0,
            isLoading: false,
            products: [],
            averageDailySales: 0,
            averageSalesRank: 0,
            averagePrice: 0,
            averageReviews: 0
        });
    }

    private async searchProducts() {
        let averageDailySales = 0;
        let averageSalesRank = 0;
        let averagePrice = 0;
        let averageReviews = 0;

        let products = await SearchProducts.search(this.state.query, this.state.currentPage);
        products.forEach(product => {
            averageDailySales += product.dailySales
            averageSalesRank += product.rank;
            averagePrice += product.price;
            averageReviews += product.reviews
        });

        averageDailySales = Math.round(averageDailySales / products.length);
        averageSalesRank = Math.round(averageSalesRank / products.length);
        averagePrice = averagePrice / products.length;
        averageReviews = Math.round(averageReviews / products.length);

        this.setState({
            isLoading: false,
            products: products,
            averageDailySales: averageDailySales,
            averageSalesRank: averageSalesRank,
            averagePrice: averagePrice,
            averageReviews: averageReviews
        });
    }

    private generateDialogHtml(name: string, price: number, stars: number, imageUrl: string) {
        let fillStarsHtml = '';
        let emptyStarsHtml = '';
        for (let j = 0; j < stars; ++j) fillStarsHtml += '<i class="fas fa-star mr-1"></i>';
        for (let j = stars; j < 5; ++j) emptyStarsHtml += '<i class="far fa-star mr-1"></i>';
        return `
            <div class="media">
                <img src="${imageUrl}" class="align-self-center mr-3" />
                <div class="media-body">
                    <p class="mb-0">${name}</p>
                    <p class="mb-0"><b>${this.currencyFormat.format(price)}</b></p>
                    <div class="pt-2 d-flex text-stars align-items-center">
                        ${fillStarsHtml}
                        ${emptyStarsHtml}
                    </div>
                </div>
            </div>
        `;
    }

    render() {
        return (
            <div id="search-component">

                <nav className="navbar fixed-top">
                    <form className="form-inline w-100">
                        <div className="input-group w-100">
                            <input type="text" className="form-control" placeholder="Search for anything" id="search-field" value={this.state.query} onKeyDown={this.onSearchEnter} onChange={this.onHandleChange} />
                            <div className="input-group-prepend">
                                {
                                    this.state.query ?
                                        <span className="input-group-text" onClick={this.onClearSearchClick}>
                                            <i className="fas fa-times"></i>
                                        </span> : <></>
                                }
                                {
                                    this.state.isLoading ?
                                        <span className="input-group-text">
                                            <i className="fas fa-sync fa-spin"></i>
                                        </span> : <></>
                                }
                            </div>
                        </div>
                    </form>
                </nav>

                <div className="page-container">
                    <div className="container-fluid">

                        <div id="averages-section">
                            <div className="row row-flex">
                                <div className="col-md-3 py-2 py-md-0">
                                    <div className="card bg-primary-300">
                                        <div className="card-body">
                                            <h3 className="card-title">
                                                {
                                                   this.state.products.length > 0 ?
                                                   <>{this.state.averageDailySales}</> : <>--</>
                                                }
                                            </h3>
                                            <p className="card-text">Average Daily Sales</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 py-2 py-md-0">
                                    <div className="card bg-warning-400">
                                        <div className="card-body">
                                            <h3 className="card-title">
                                                {
                                                   this.state.products.length > 0 ?
                                                   <>{this.state.averageSalesRank}</> : <>--</>
                                                }
                                            </h3>
                                            <p className="card-text">Average Sales Rank</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 py-2 py-md-0">
                                    <div className="card bg-success-200">
                                        <div className="card-body">
                                            <h3 className="card-title">
                                                {
                                                   this.state.products.length > 0 ?
                                                   <>{this.currencyFormat.format(this.state.averagePrice)}</> : <>--</>
                                                }
                                            </h3>
                                            <p className="card-text">Average Price</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 py-2 py-md-0">
                                    <div className="card bg-info-200">
                                        <div className="card-body">
                                            <h3 className="card-title">
                                                {
                                                   this.state.products.length > 0 ?
                                                   <>{this.state.averageReviews}</> : <>--</>
                                                }
                                            </h3>
                                            <p className="card-text">Average Reviews</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="results-section" className="mt-4">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="table-responsive-md">
                                        <table className="table table-bordered m-0">
                                            <thead className="bg-warning-200">
                                                <tr>
                                                    <th scope="col" className="text-center">#</th>
                                                    <th scope="col">Product Name</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Rank</th>
                                                    <th scope="col">Mo. Sales</th>
                                                    <th scope="col">D. Sales</th>
                                                    <th scope="col">Revenue</th>
                                                    <th scope="col">Reviews</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.products.map((product: ProductModel, index: any) =>
                                                        <tr key={index}>
                                                            <td className="text-center">{ index + 1 }</td>
                                                            <td>
                                                                <button type="button" className="btn btn-link" data-html="true" data-placement="top" data-toggle="popover"
                                                                    data-content={this.generateDialogHtml(product.name, product.price, product.stars, product.imageUrl)}>
                                                                    {product.name}
                                                                </button>
                                                            </td>
                                                            <td className="text-orange">{this.currencyFormat.format(product.price)}</td>
                                                            <td className="text-orange">#{product.rank}</td>
                                                            <td className="text-orange">{product.monthlySales}</td>
                                                            <td className="text-orange">{product.dailySales}</td>
                                                            <td>{this.currencyFormat.format(product.revenue)}</td>
                                                            <td>{product.reviews}</td>
                                                        </tr>
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <nav className="navbar fixed-bottom">
                    <div className="row">
                        <div className="col-md-4 text-center text-md-left align-self-center">
                            {
                                this.state.products.length > 0 ?
                                <button type="button" className="btn btn-link" onClick={this.onNextPageClick}>Extract next page >></button> : <></>
                            }
                        </div>
                        <div className="col-md-4 text-center text-md-center align-self-center">
                            {
                                this.state.products.length > 0 ?
                                <>Showing results 1 - {this.state.products.length}</> : <></>
                            }
                        </div>
                        <div className="col-md-4 text-center text-md-right align-self-center">
                            <i className="far fa-question-circle"></i>
                        </div>
                    </div>
                </nav>

            </div >
        )
    }
}
