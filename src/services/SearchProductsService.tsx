import axios from 'axios';
import ProductModel from './../models/ProductModel'
import { environment } from './../environments/environment';

class SearchProductsService {

    async search(query: string, page: number): Promise<ProductModel[]> {
        const data = await axios.get(`${ environment.apiUrl }/search?k=${ query }&page=${ page }`);
        let results: ProductModel[] = (data.data || []).map((product: ProductModel) => {
            return {
                asin: product.asin,
                name: product.name,
                brand: product.brand,
                price: product.price,
                category: product.category,
                rank: product.rank,
                monthlySales: product.monthlySales,
                dailySales: product.dailySales,
                revenue: product.revenue,
                reviews: product.reviews,
                seller: product.seller,
                imageUrl: product.imageUrl,
                description: product.description,
                stars: product.stars
            };
        }).filter((product: ProductModel) => { return product.price });
        return results;
    }
    
}

export const SearchProducts = new SearchProductsService()